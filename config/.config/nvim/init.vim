set expandtab
set shiftwidth=2
set softtabstop=2
set number

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files']
let g:airline_theme='onedark'
let g:airline_powerline_fonts = 1

call plug#begin('~/.local/share/nvim/plugged')

Plug 'mxw/vim-jsx'
Plug 'elixir-editors/vim-elixir'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'slim-template/vim-slim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()
